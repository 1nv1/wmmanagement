const util = require('util');

var io_mng = require('socket.io-client');
var socket_mng = io_mng('http://localhost:9696');

socket_mng.on('connect', () => {
	console.log(`CONNECTED`);
})
.on('response', (data) => {
	console.log(util.inspect(data, {showHidden: true, depth: null}));
});

var WMState = {
  ONLINE: 0,
  OFFLINE: 1,
  RUNNING: 2,
  RECONNECT: 3
};


var io_wm = require('socket.io-client');
var socket_wm = io_wm('http://localhost:9696', {
	timeout: 500
});

console.log(`to: ${socket_wm.timeout}`);

socket_wm.on('connect', () => {
	console.log(`CONNECTED`);
})
.on('response', (data) => {
	console.log(`response`);
	console.log(util.inspect(data, {showHidden: true, depth: null}));
});




var card_id = '123123123123';
var device_id = '12345678';

socket_wm.emit('wm_register', {'deviceID' : device_id}, (err) => {
	console.log(`wm_register`);
	console.log(err);

	socket_mng.emit('mng_request_run', {'cardID' : card_id }, (err) => {
		console.log(`mng_request_run`);
		console.log(err);

		setTimeout(() => {
			socket_wm.emit('wm_ask_for_run', {'cardID' : card_id, 'deviceID' : device_id }, (err) => {
				console.log(`wm_ask_for_run`);
				console.log(err);

				// setTimeout(() => {
				// 	socket_wm.close();
				// 	socket_mng.close();
				// }, 2000);

				setTimeout(() => {
					socket_wm.emit('wm_stop', {'deviceID' : device_id }, (err) => {
						console.log(`wm_stop`);
						console.log(err);

						setTimeout(() => {
							socket_wm.close();
							socket_mng.close();
						}, 2000);
					});
				}, 2000);

			});
		}, 2000);

	});

});


