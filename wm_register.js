const util = require('util');

var WMState = {
  ONLINE: 0,
  OFFLINE: 1,
  RUNNING: 2,
  RECONNECT: 3
};


var io_wm = require('socket.io-client');
var socket_wm = io_wm('http://localhost:9696', {
    timeout: 500
});

console.log(`to: ${socket_wm.timeout}`);

socket_wm.on('connect', () => {
    console.log(`CONNECTED`);
})
.on('response', (data) => {
    console.log(`response`);
    console.log(util.inspect(data, {showHidden: true, depth: null}));
});




// var card_id = '00100';
var device_id = '10001';

socket_wm.emit('wm_register', {'deviceID' : device_id}, (err) => {
    console.log(`wm_register`);
    console.log(err);
});


