var express = require('express');
var _g_router = express.Router();
var path = require('path');
const util = require('util');

var WMState = {
  ONLINE: 0,
  OFFLINE: 1,
  RUNNING: 2,
  RECONNECT: 3
};


var _g_sqlite3 = require('sqlite3').verbose();
var _g_db_file = "temp.bin";
/* GET home page. */
_g_router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', 'views', 'index.html'));
});

_g_router.get('/get_users', function(req,res) {
    if(!req.query || (!req.query.name && !req.query.phone)) return;
    var responseData = [];
    var _l_db = new _g_sqlite3.Database(_g_db_file);

    _l_db.serialize(function() {
        var sql_query;

        if(req.query.name)
        {
            sql_query = `SELECT user_id, name, phone FROM User WHERE name LIKE '%${req.query.name}%'`;
        }
        else
        {
            sql_query = `SELECT user_id, name, phone FROM User WHERE phone LIKE '%${req.query.phone}%'`;
        }

        _l_db.each(`${sql_query}`, function(_a_err, _a_row) {
                if(_a_err)
                {
                    console.log(_a_err);
                }
                responseData.push({ user_id: _a_row.user_id, name : _a_row.name, phone : _a_row.phone });
            }, function() {
                res.json(responseData);
                res.end();
            });

        _l_db.close(() => { console.log(`closed`); });
    }, function() {
        res.json(responseData);
        res.end();
    });
})
.get('/history', function(req,res) {
    if(!req.query || (!req.query.from && !req.query.to)) return;
    console.log(`Recevied history request: from ${req.query.from} to ${req.query.to}`);
    var responseData = [];
    var _l_db = new _g_sqlite3.Database(_g_db_file);

    _l_db.serialize(function() {

        // TODO
        var sql_query = `SELECT FactUsed.card_id, FactUsed.wm_id, User.name, User.phone, FactUsed.duration, FactUsed.cost, FactUsed.is_paid, FactUsed.used_at FROM (FactUsed inner join User on User.user_id=FactUsed.user_id) WHERE (is_paid=1) OR (used_at BETWEEN DATETIME(${req.query.from}, 'unixepoch') AND DATETIME(${req.query.to}, 'unixepoch'))`;

        _l_db.each(`${sql_query}`, function(_a_err, _a_row) {
                if(_a_err)
                {
                    console.log(_a_err);
                }
                // console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
                responseData.push(_a_row);
            }, function() {
                res.json(responseData);
                res.end();
            });

        _l_db.close(() => { console.log(`closed`); });

    }, function() {
        res.json(responseData);
        res.end();
    });
});

exports.index = _g_router;

exports.tmpl = function (req, res) {
  var name = req.params.name;
  res.sendFile(path.join(__dirname, '../', 'views', name));
};

exports.history = function (req, res) {
  var name = req.params.name;
  res.sendFile(path.join(__dirname, '../', 'views', name));
};
