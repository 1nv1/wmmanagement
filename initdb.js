const util = require('util');
var sqlite3 = require('sqlite3').verbose();
var file = "temp.bin";

var wmListID = [];

Object.defineProperty(global, '__stack', {
get: function() {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
get: function() {
        return __stack[1].getLineNumber();
    }
});

Object.defineProperty(global, '__function', {
get: function() {
        return __stack[1].getFunctionName();
    }
});

Object.defineProperty(global, '__file', {
get: function() {
        return __stack[1].getFileName();
    }
});

function execute_sql(_a_db_name, _a_query, _a_call_back)
{
    let _l_db = new sqlite3.Database(_a_db_name);
    // _l_db.configure("busyTimeout", 3000);

    _l_db.serialize(() =>
    {

        console.log(`[SQL] ${_a_db_name} : ${_a_query}`);

        _l_db.run(`${_a_query}`, function(_a_err)
        {
            if(_a_err)
            {
                console.log(`[ERR] ${_a_err}`);
                if(_a_call_back) _a_call_back({ log: 'Error: SQLITE_BUSY: database is locked', errno: 5, code: 'SQLITE_BUSY' });
            }
            else if(_a_call_back)
            {
                _a_call_back(null, this.lastID);
            }
        });

        _l_db.close();
    });
}

var _g_row_id;
var _g_db = new sqlite3.Database(file);


_g_db.serialize(function() {

  // User
  _g_db.run("CREATE TABLE if not exists User (user_id INTEGER PRIMARY KEY AUTOINCREMENT, name NCHAR(50) NOT NULL, phone VARCHAR(15) NOT NULL)");

  _g_db.run(`INSERT INTO User (name, phone) VALUES ('Le Thanh Long', 0123456789)`);
  _g_db.run(`INSERT INTO User (name, phone) VALUES ('Nguyen Duy Hung', 7897897890)`);
  _g_db.run(`INSERT INTO User (name, phone) VALUES ('Nguyen Nhat Anh', 1112223334)`);

  _g_db.each("SELECT * FROM User", function(_a_err, _a_row) {
    if(_a_err) console.log(_a_err);
    else
    {
      console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
    }
  }, function() {
    // process.exit(1);
  });


  // User_WashingMachine_Cost
  _g_db.run("CREATE TABLE if not exists FactUsed (fact_used_id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, cost, duration INTEGER NOT NULL, card_id VARCHAR(15) NOT NULL, wm_id VARCHAR(15), used_at DATETIME, is_paid BOOLEAN NOT NULL DEFAULT 0, is_done BOOLEAN NOT NULL DEFAULT 0, FOREIGN KEY(user_id) REFERENCES User(user_id))");

  _g_db.run(`INSERT INTO FactUsed (user_id, cost, duration, card_id) VALUES (1, 50000, 40, '00100')`);
  _g_db.run(`INSERT INTO FactUsed (user_id, cost, duration, card_id) VALUES (2, 10000, 40, '00200')`);
  let _l_datetime = (new Date().getTime()) / 1000;
  console.log(_l_datetime);
  _g_db.run(`INSERT INTO FactUsed (user_id, cost, duration, card_id, wm_id, used_at) VALUES (3, 20000, 40, '00110', '10001', DATETIME(${_l_datetime}, 'unixepoch', '-3 day'))`);
  _g_db.run(`INSERT INTO FactUsed (user_id, cost, duration, card_id, is_paid) VALUES (2, 30000, 40, '00101', 1)`);
  _g_db.run(`INSERT INTO FactUsed (user_id, cost, duration, card_id, wm_id, used_at, is_paid, is_done) VALUES (2, 100000, 80, '00111', '10002', DATETIME(${_l_datetime}, 'unixepoch'), 1, 1)`);

  // select static/dynamic card
  // _g_db.each("SELECT FactUsed.fact_used_id, FactUsed.user_id, User.name, User.phone, FactUsed.wm_id, cost, duration, FactUsed.card_id, FactUsed.used_at, FactUsed.is_paid FROM (FactUsed INNER JOIN User ON User.user_id = FactUsed.user_id) WHERE FactUsed.is_done = 0", function(_a_err, _a_row) {
  //   if(_a_err) console.log(_a_err);
  //   else
  //   {
  //     console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
  //   }
  // }, function() {

  // });

  // _g_db.each("SELECT card_id, is_paid, used_at FROM FactUsed WHERE fact_used_id=3", function(_a_err, _a_row) {
  //   if(_a_err) console.log(_a_err);
  //   else
  //   {
  //     console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
  //   }
  // }, function() {

  // });
  // _g_db.run("CREATE TABLE if not exists washing_machine_used (id INTEGER PRIMARY KEY AUTOINCREMENT, wm_id INTEGER NOT NULL, used_at DATETIME NOT NULL, duration INTEGER, UNIQUE(wm_id) ON CONFLICT REPLACE)");

  // _g_db.run(`INSERT INTO washing_machine_used (wm_id, used_at, duration) VALUES ('12345678', 5000, 5000)`);

  // _g_db.each("SELECT * FROM washing_machine_used", function(_a_err, _a_row) {
  //   if(_a_err) console.log(_a_err);
  //   else
  //   {
  //     console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
  //   }
  // }, function() {
  // });


  // _g_db.run("CREATE TABLE if not exists user_used (id INTEGER PRIMARY KEY AUTOINCREMENT, name NCHAR(50) NOT NULL, phone VARCHAR(15) NOT NULL, card_id VARCHAR(50) NOT NULL, duration INTEGER NOT NULL, is_used BOOLEAN NOT NULL, is_paid BOOLEAN NOT NULL, UNIQUE(card_id) ON CONFLICT REPLACE)");

  // _g_db.run(`INSERT INTO user_used (name, phone, card_id, duration, is_used, is_paid) VALUES ('row 1', '0 0', '000111222333', 35, 0, 0)`);
  // _g_db.run(`INSERT INTO user_used (name, phone, card_id, duration, is_used, is_paid) VALUES ('row 2', '0 0', '000111222333', 35, 0, 0)`);
  // _g_db.run(`INSERT INTO user_used (name, phone, card_id, duration, is_used, is_paid) VALUES ('row 3', '1 0', '123123123123', 35, 1, 0)`);
  // _g_db.run(`INSERT INTO user_used (name, phone, card_id, duration, is_used, is_paid) VALUES ('row 4', '0 1', '111222333444', 35, 0, 1)`, function(err) {

  //   _g_row_id = this.lastID;
  //   console.log(_g_row_id);
  //   _g_db.serialize(function() {
  //     // _g_db.run(`UPDATE user_used SET name='HEHEHE', is_paid=1 WHERE id=${_g_row_id}`);

  //     _g_db.each("SELECT name, phone, card_id, duration, is_used, is_paid FROM user_used", function(_a_err, _a_row) {
  //       if(_a_err) console.log(_a_err);
  //       else
  //       {
  //         console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
  //       }
  //     }, function() {
  //     });


      _g_db.close();
  //   });

  // });


});



