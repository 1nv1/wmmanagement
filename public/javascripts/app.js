// const util = require('util');

var app = angular.module('WMApp',['smart-table', 'ngAria', 'ngAnimate', 'ngMaterial']);
var socket = io.connect('http://localhost:9696', {
    timeout: 500,
    reconnection: true
});

app.controller('MainCtrl', ['$scope', '$http', '$mdDialog', '$timeout', '$q', function ($scope, $http, $mdDialog, $timeout, $q) {

    $scope.wm_rc = [];
    $scope.wm_dc = [];
    // $scope.fact_used  = {duration : 40};

    $scope.fact_used_rc = [];
    $scope.fact_used_dc = [];

    $scope.history_rc = [];
    $scope.history_dc = [];

    // $scope.rp_to = new Date();
    $scope.rp_to = new Date(new Date().toDateString());
    this.to_day = $scope.rp_to;
    $scope.rp_from = new Date($scope.rp_to.getTime() - ($scope.rp_to.getDay() * 86400));
    $scope.total_duration = 0;
    $scope.total_cost = 0;

    $scope.get_history = function() {
        // console.log(new Date($scope.rp_from.toDateString()));
        console.log($scope.rp_to);
        console.log($scope.rp_from);

        let request = $http.get('/history', {params: {from: Math.round($scope.rp_from.getTime() / 1000), to: Math.round($scope.rp_to.getTime() / 1000) + (86400)} });
        request.then(function(_a_data) {
            $scope.history_rc = _a_data.data;
            $scope.total_duration = 0;
            $scope.total_cost = 0;
            for(var i in _a_data.data)
            {
                if(_a_data.data[i].used_at)
                {
                    $scope.total_duration += _a_data.data[i].duration;
                }
                if(_a_data.data[i].is_paid)
                {
                    $scope.total_cost += _a_data.data[i].cost;
                }
            }

            $scope.history_dc = [].concat($scope.history_rc);
        }, function(_a_data){
            console.log('Error: ' + _a_data);
        });
    }

    $scope.get_history();

    socket.on('html_initialize', function(_a_data) {
        console.log(_a_data);

        if(_a_data.wm_list)
        {
            $scope.wm_rc = _a_data.wm_list;
            $scope.wm_dc = [].concat($scope.wm_rc);
        }

        if(_a_data.card_list)
        {
            $scope.fact_used_rc = _a_data.card_list;
            $scope.fact_used_dc = [].concat($scope.fact_used_rc);
        }

    });

    // Incoming
    socket.on('html_bc_wm_state_change', function(_a_data) {
        console.log(_a_data);

        if(_a_data.card_info)
        {
            for (var i = $scope.fact_used_rc.length - 1; i >= 0; i--)
            {
                if($scope.fact_used_rc[i]._m_card_id === _a_data.card_info.card_id)
                {
                    if($scope.fact_used_rc[i]._m_is_paid)
                    {
                        console.log('delete: ' + i);
                        $scope.fact_used_rc.splice(i, 1);
                    }
                    else
                    {
                        console.log('used');
                        console.log($scope.fact_used_rc[i]);
                        $scope.fact_used_rc[i]._m_used_at = _a_data.card_info.used_at;
                        $scope.fact_used_rc[i]._m_wm_id = _a_data.card_info.wm_id;
                        console.log($scope.fact_used_rc[i]);
                    }

                    $scope.$apply(function () {
                        $scope.fact_used_dc = [].concat($scope.fact_used_rc);
                    });

                    $scope.get_history();
                    break;
                }
            }

            console.log($scope.fact_used_rc);
        }

        var wm_obj = _a_data.wm_info;
        if($scope.wm_rc)
        {
            for(var i=0; i<$scope.wm_rc.length; i++)
            {
                if($scope.wm_rc[i]._m_wm_id == wm_obj.wm_id)
                {
                    $scope.$apply(function () {
                        $scope.wm_rc[i]._m_state = wm_obj.state;
                        if(wm_obj.last_run)
                        {
                            $scope.wm_rc[i]._m_last_run = wm_obj.last_run;
                            // $scope.wm_rc[i]._m_running_count++;
                        }
                        // if(wm_obj.running_time) $scope.wm_rc[i]._m_total_running_time += wm_obj.running_time;
                        $scope.wm_dc = [].concat($scope.wm_rc);
                    });
                    return;
                }
            }
        }

        console.log('push');
        // new WM, first time connect to server.
        $scope.$apply(function () {
            $scope.wm_rc.push({
                _m_wm_id: wm_obj.wm_id,
                _m_state: wm_obj.state,
            });
            $scope.wm_dc = [].concat($scope.wm_rc);
        });
    });

    socket.on('html_bc_play_sound', function(_a_data) {
        // console.log(_a_data);
        // var audio = new Audio(_a_data.file);
        // audio.play();
    });

    socket.on('html_bc_card_register', function(_a_data) {
        console.log(_a_data);

        var _l_row;

        for (var i = $scope.fact_used_rc.length - 1; i >= 0; i--) {
            if($scope.fact_used_rc[i]._m_card_id == _a_data.card_id)
            {
                _l_row = $scope.fact_used_rc[i];
                console.log(_l_row);
                $scope.show_dialog(_l_row);
                return;
            }
        }

        console.log(_l_row);

        $scope.show_dialog({
            _m_fact_used_id     : undefined,
            _m_user_id          : undefined,
            // _m_name             : undefined,
            // _m_phone            : undefined,
            _m_wm_id            : undefined,
            _m_cost             : undefined,
            _m_duration         : 40,
            _m_card_id          : _a_data.card_id,
            _m_used_at          : undefined,
            _m_is_paid          : 0,
        });
    });

    $scope.show_alert = function(_a_title, _a_message) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog
        $mdDialog.show(
            $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(_a_title)
            .textContent(_a_message)
            // .ariaLabel('Alert Dialog Demo')
            .ok('Close')
            // .targetEvent(ev)
        );
    };

    $scope.show_dialog = function(_a_row) {
        console.log(_a_row);
        var _l_index = $scope.fact_used_rc.indexOf(_a_row);

        var _l_fact_used =
        {
            _m_fact_used_id     : _a_row._m_fact_used_id,
            _m_user_id          : _a_row._m_user_id,
            _m_name             : _a_row._m_name,
            _m_phone            : _a_row._m_phone,
            _m_wm_id            : _a_row._m_wm_id,
            _m_cost             : _a_row._m_cost,
            _m_duration         : _a_row._m_duration,
            _m_card_id          : _a_row._m_card_id,
            _m_used_at          : _a_row._m_used_at,
            _m_is_paid          : _a_row._m_is_paid,
        }

        $mdDialog.show({
            controller: dialog_controller,
            controllerAs: '_m_ctrl',
            templateUrl: '/tmpl/payment.tmpl.html',
            parent: angular.element(document.body),
            // isolateScope: false,
            // targetEvent: ev,
            // clickOutsideToClose:true,
            locals: {
                _a_fact_used: _l_fact_used,
                // _a_row_index : _l_index,
            },
            // _a_main_scope : $scope,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function(_a_data) {

            console.log(_l_index);
            console.log(_a_data.fact_used);
            var _l_fact_used = _a_data.fact_used;
            var _l_is_paid = _a_data.type;

            if(!_l_fact_used) return;

            // create new user
            if(!_l_fact_used._m_user_id)
            {

                socket.emit('browser_save_user', _l_fact_used , (_a_err, _a_data) => {
                    console.log(_a_err + " : " + _a_data);
                    if(_a_err) { $scope.show_alert('ERROR', 'Failed to save user'); return; }

                    if(_a_data) _l_fact_used._m_user_id = _a_data;

                    if(_l_index == -1)
                    {
                        socket.emit('browser_insert_card', _l_fact_used , (_a_err, _a_data) => {
                            console.log(_a_err + " : " + _a_data);

                            if(_a_err) { $scope.show_alert('ERROR', 'Failed to insert card'); return; }

                            if(_a_data) _l_fact_used._m_fact_used_id = _a_data;

                            $scope.fact_used_rc.push(_l_fact_used);
                            $scope.$apply(function () {
                                $scope.fact_used_dc = [].concat($scope.fact_used_rc);
                            });
                        });
                    }
                    else
                    {
                        socket.emit('browser_update_card', _l_fact_used , (_a_err, _a_data) => {
                            console.log(_a_err + " : " + _a_data);

                            if(_a_err) { $scope.show_alert('ERROR', 'Failed to update card'); return; }

                            $scope.fact_used_rc[_l_index] = _l_fact_used;
                            $scope.$apply(function () {
                                $scope.fact_used_dc = [].concat($scope.fact_used_rc);
                            });
                        });
                    }
                });

            }
            // insert fact_used
            else if(_l_index == -1)
            {
                if(_l_is_paid) _l_fact_used._m_is_paid = 1;

                socket.emit('browser_insert_card', _l_fact_used , (_a_err, _a_data) => {
                    console.log(_a_err + " : " + _a_data);

                    if(_a_err) { _l_fact_used._m_is_paid = 0; $scope.show_alert('ERROR', 'Failed to insert card'); return; }

                    if(_a_data) _l_fact_used._m_fact_used_id = _a_data;

                    $scope.fact_used_rc.push(_l_fact_used);
                    $scope.$apply(function () {
                        $scope.fact_used_dc = [].concat($scope.fact_used_rc);
                    });

                    // print and update history
                    if(_l_is_paid)
                    {
                        l_print(_l_fact_used);
                        $scope.get_history();
                    }
                });
            }
            // update fact_used
            else
            {
                if(_l_is_paid) _l_fact_used._m_is_paid = 1;

                socket.emit('browser_update_card', _l_fact_used , (_a_err, _a_data) => {
                    console.log(_a_err + " : " + _a_data);

                    if(_a_err) { _l_fact_used._m_is_paid = 0; $scope.show_alert('ERROR', 'Failed to update card'); return; }

                    // if is used already
                    if(_l_fact_used._m_used_at)
                    {
                        $scope.fact_used_rc.splice(_l_index, 1);
                    }
                    else
                    {
                        $scope.fact_used_rc[_l_index] = _l_fact_used;
                    }

                    $scope.$apply(function () {
                        $scope.fact_used_dc = [].concat($scope.fact_used_rc);
                    });

                    // print and update history
                    if(_l_is_paid)
                    {
                        l_print(_l_fact_used);
                        $scope.get_history();
                    }
                });
            }
        }, function() {
            console.log();
        });
    };

    function dialog_controller($timeout, $q, $scope, $mdDialog, $http, _a_fact_used) {


        $scope.dialog_cancel = function() {
            console.log('cancel');
            $mdDialog.cancel();
        };

        $scope.dialog_save = function(_a_fact_used)
        {
            console.log(_a_fact_used);
            $mdDialog.hide({type : 0 , fact_used: _a_fact_used});
        };

        $scope.dialog_paid = function(_a_fact_used)
        {
            $mdDialog.hide({type : 1 , fact_used: _a_fact_used});
        }

        var self = this;
        self.fact_used = _a_fact_used;

        if(self.fact_used._m_name)
        {
            self.selectedUser = {
                name: self.fact_used._m_name,
                phone: self.fact_used._m_phone || 0,
                user_id: self.fact_used._m_user_id,
            };
        }
        else
        {
            self.selectedUser = undefined;
        }


        console.log(self.selectedUser);

        self.searchTextName = null;
        self.searchTextPhone = null;
        self.searchName = searchName;
        self.searchPhone = searchPhone;

        self.selectedItemChange = selectedItemChange;
        self.searchTextChange = searchTextChange;

        function searchName (query) {
            var deferred = $q.defer();
            $http.get('/get_users', {params: {name: query}  })
                .then(function (response)
                {
                    console.log(response.data);
                    // if(response.data && !response.data.phone) response.data.phone = '0123456789';
                    deferred.resolve(response.data);
                });
            return deferred.promise;
        }

        function searchPhone (query) {
            return $http.get('/get_users', {params: {phone: query}  })
                .then(function (response)
                {
                     return response.data;
                });
        }

        function selectedItemChange(item, type) {
            console.log(item);
            self.selectedUser = item;
            // if(self.selectedUser && !self.selectedUser.phone) self.selectedUser.phone = '';
            // self.fact_used = item;
            if(item)
            {
                self.fact_used._m_phone = item.phone;
                self.fact_used._m_name = item.name;
                self.fact_used._m_user_id = item.user_id;
            }
            else
            {
                self.fact_used._m_user_id = undefined;
            }
            // console.log(self.fact_used);
            // console.log(self.selectedUser);
        }

        function searchTextChange(text, type) {
            console.log(self.fact_used);
            if(type)
            {
                self.fact_used._m_phone = text;
            }
            else
            {
                self.fact_used._m_name = text;
            }
        }

    }

    $scope.remove_card = function remove_card(_a_row) {
        var _l_index = $scope.fact_used_rc.indexOf(_a_row);
        if (_l_index !== -1)
        {
            var _l_fact_used = _a_row;
            console.log('delete: ' + _l_index);
            // $scope.fact_used_rc.splice(_l_index, 1);
            socket.emit('browser_delete_card', _l_fact_used , (_a_err, _a_data) => {
                console.log(_a_err + " : " + _a_data);
                console.log(_l_fact_used);

                if(_a_err) { $scope.show_alert('ERROR', 'Failed to delete card'); return; }

                // remove
                $scope.fact_used_rc.splice(_l_index, 1);

                $scope.$apply(function () {
                    $scope.fact_used_dc = [].concat($scope.fact_used_rc);
                });
            });
        }
    }

    $scope.btn_archive = function () {
        let _l_temp = [].concat($scope.history_dc);
        _l_temp.push({
                card_id: 'TOTAL',
                wm_id: '',
                name: '',
                phone: '',
                used_at: '',
                duration: $scope.total_duration,
                is_paid: 1,
                cost: $scope.total_cost,
            });
        alasql(`SELECT card_id AS CardID, IFNULL(wm_id,'') AS WashingMachineID, name AS Name, phone AS Phone, IFNULL(used_at,'') AS UsedAt, duration AS Duration, is_paid AS Paid, cost AS Cost INTO XLSX('Report_${$scope.rp_from.toISOString().slice(0, 10)}_${$scope.rp_to.toISOString().slice(0, 10)}.xlsx',{headers:true}) FROM ?`,[_l_temp]);
    };

    l_print = function(_a_fact_used)
    {
        console.log(`${_a_fact_used}`);
        var popupWin = window.open('', '_blank', 'width=800,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" href="angular-material/angular-material.min.css"> <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"> <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> <link rel="stylesheet" href="stylesheets/style.css"> </head><body onload="window.print()">' + '<div style="max-width: 10cm;">\
<md-content layout-xs="column" layout="row">\
    <div flex-xs layout="column" >\
      <md-card>\
        <md-card-title>\
          <md-card-title-text>\
            <span class="md-headline">Cô Tấm Laundry</span>\
          </md-card-title-text>\
        </md-card-title>\
        <img src="resources/cotam_logo.png" class="md-card-image" alt="Washed Out">\
        <md-card-content>\
          <md-list>\
            <md-list-item class="md-1-line" style="min-height: 2cm;">\
              <span><md-icon class="material-icons md-18 black" style="margin-right: 5px;">person</md-icon>' + _a_fact_used._m_name + '</span>\
              <md-divider></md-divider>\
            </md-list-item>\
            <md-list-item class="md-1-line" style="min-height: 2cm;">\
              <span><md-icon class="material-icons md-18 black" style="margin-right: 5px;">phone</md-icon>' + _a_fact_used._m_phone + '</span>\
              <md-divider></md-divider>\
            </md-list-item>\
            <md-list-item class="md-1-line" style="min-height: 2cm;">\
              <span><md-icon class="material-icons md-18 black" style="margin-right: 5px;">access_time</md-icon>' + _a_fact_used._m_duration + ' minutes</span>\
              <md-divider></md-divider>\
            </md-list-item>\
            <md-list-item class="md-1-line" style="min-height: 2cm;">\
              <span><md-icon class="material-icons md-18 black" style="margin-right: 5px;">monetization_on</md-icon>'+ (_a_fact_used._m_cost) +' VND</span>\
            </md-list-item>\
          </md-list>\
        </md-card-content>\
      </md-card>\
    </div>\
  </md-content>\
</div>'            + '</body></html>');
        popupWin.document.close();
    }

}]);

app.filter('to_time_string', function() {
    return function(millseconds, multiplier) {

        if(multiplier == undefined) multiplier = 1;

        millseconds = millseconds * multiplier;

        var oneSecond = 1000;
        var oneMinute = oneSecond * 60;
        var oneHour = oneMinute * 60;
        var oneDay = oneHour * 24;
        var oneWeek = oneDay * 7;

        var seconds = Math.floor((millseconds % oneMinute) / oneSecond);
        var minutes = Math.floor((millseconds % oneHour) / oneMinute);
        var hours = Math.floor((millseconds % oneDay) / oneHour);
        var days = Math.floor(millseconds / oneDay);
        var weeks = Math.floor(millseconds / oneWeek);


        var timeString = '';
        if (weeks !== 0) {
            timeString += (weeks + ' w ');
        }
        if (days !== 0) {
            timeString += (days + ' d ');
        }
        if (hours !== 0) {
            timeString += (hours + ' h ');
        }
        if (minutes !== 0) {
            timeString += (minutes + ' m ');
        }
        if (seconds !== 0 || millseconds < 1000) {
            timeString += (seconds + ' s ');
        }

        return timeString;
    };
});

// app.directive('wmCard', function () {
//     return {
//         restrict: 'E',
//             templateUrl: '/tmpl/wm_card.tmpl.html',
//             scope: {
//                 name: '@',
//                 theme: '@'
//             },
//         controller: function ($scope) {
//             $scope.theme = $scope.theme || 'default';
//         }
//     }
// });

// app.directive('factUsedItem', function () {
//     return {
//         restrict: 'E',
//             templateUrl: '/tmpl/fact_used_item.tmpl.html',
//             // scope: {
//             // //     name: '@',
//             //     fact_used_item: '=',
//             // },
//         controller: function ($scope) {
//             // $scope.fact_used_dc = $scope.fact_used_dc || 'default';
//         }
//     }
// });
