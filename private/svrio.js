const util = require('util');

var WMState = {
  ONLINE: 0,
  OFFLINE: 1,
  RUNNING: 2,
  RECONNECT: 3
};

var WMResCode = {
  OK: 0,
  NOK: 1,
  ALOWRUN: 2,
  DECLINERUN: 3,
};

function createWM()
{
    return {
        _m_wm_id                : undefined,
        _m_state                : WMState.ONLINE,
        _m_last_run            : undefined,
    };
}

function execute_sql(_a_db_name, _a_query, _a_callback)
{
    let _l_db = new _g_sqlite3.Database(_a_db_name);
    _l_db.configure("busyTimeout", 3000);

    _l_db.serialize(function() {

        console.log(`[SQL] ${_a_query}`);

        _l_db.run(`${_a_query}`, function(_a_err)
        {
            if(_a_err)
            {
                console.log(`[ERR] ${_a_err}`);
            }

            if(_a_callback) _a_callback(_a_err, this.lastID);
        });

        _l_db.close();
    });
}

var _g_wm_list = {};
var _g_wm_sock_list = {};
var _g_cards = {};

// var _g_wm_arr = [];
// var _g_card_arr = [];

////////////////////////////SQLITE3////////////////////////////////////////////
var _g_sqlite3 = require('sqlite3').verbose();
var _g_db_file_name = "wm.bin";

{
  let _l_db = new _g_sqlite3.Database(_g_db_file_name);

  _l_db.serialize(function() {

    // User
    _l_db.run("CREATE TABLE if not exists User (user_id INTEGER PRIMARY KEY AUTOINCREMENT, name NCHAR(50) NOT NULL, phone VARCHAR(15) NOT NULL)");

    _l_db.run("CREATE TABLE if not exists FactUsed (fact_used_id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, cost, duration INTEGER NOT NULL, card_id VARCHAR(15) NOT NULL, wm_id VARCHAR(15), used_at DATETIME, is_paid BOOLEAN NOT NULL DEFAULT 0, is_done BOOLEAN NOT NULL DEFAULT 0, FOREIGN KEY(user_id) REFERENCES User(user_id))");

    // // fill into wm_list
    // _l_db.each("SELECT wm_id, name FROM WashingMachine", function(_a_err, _a_row) {
    //     if(_a_err)
    //     {
    //         console.log(`[ERR] _a_err`);
    //     }
    //     else
    //     {
    //         _g_wm_list[_a_row.wm_id] = {
    //             _m_wm_id                : _a_row.wm_id,
    //             _m_wm_name              : _a_row.name,
    //             _m_state                : WMState.OFFLINE,
    //             _m_sock_id              : 0,
    //             _m_fact_used_id         : undefined,
    //         }
    //     }

    // }, function() {
    // });

    // select all dynamic/static cards
    _l_db.each("SELECT FactUsed.fact_used_id, FactUsed.user_id, User.name, User.phone, FactUsed.wm_id, cost, duration, FactUsed.card_id, FactUsed.used_at, FactUsed.is_paid FROM (FactUsed INNER JOIN User ON User.user_id = FactUsed.user_id) WHERE FactUsed.is_done = 0", function(_a_err, _a_row) {

      if(_a_err) console.log(`[ERR] _a_err`);
      else
      {
        console.log(`${__file} [${__line}] [DBG]`);

        _g_cards[_a_row.card_id] = {
            _m_fact_used_id     : _a_row.fact_used_id,
            _m_user_id          : _a_row.user_id,
            _m_name             : _a_row.name,
            _m_phone            : _a_row.phone,
            _m_wm_id            : _a_row.wm_id,
            // _m_cost_id          : _a_row.cost_id,
            _m_cost             : _a_row.cost,
            _m_duration         : _a_row.duration,
            _m_card_id          : _a_row.card_id,
            _m_used_at          : _a_row.used_at,
            _m_is_paid          : _a_row.is_paid,
        }

        console.log(util.inspect(_g_cards[_a_row.card_id], {showHidden: true, depth: null}));

      }
    }, function() {

      // console.log(`${__file} [${__line}] _g_card_arr.length: ${_g_card_arr.length}`);
      // console.log(util.inspect(_g_cards, {showHidden: true, depth: null}));

    });

    console.log(`${__file} [${__line}] [INF] Initialized`);

    _l_db.close();
  });
}

var svrio = (_a_socket) => {
    console.log(`${__file} [${__line}] CONNECTED: ${_a_socket.request.connection.remoteAddress} : ${_a_socket.request.connection.remotePort}`);
    _a_socket.emit('html_initialize', {
        card_list   : Object.keys(_g_cards).map((key)=>{ return _g_cards[key]; }),
        wm_list     : Object.keys(_g_wm_list).map((key)=>{ return _g_wm_list[key]; })
    });

    _a_socket.on('wm_register', function(_a_data, _a_callback)
    {
        console.log(`${__file} [${__line}] REGISTER: wm-${_a_data.deviceID} - state: ${_a_data.operate_state}`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        // if wm_id is new
        if(!_g_wm_list[_a_data.deviceID])
        {
            console.log(`${__file} [${__line} wm-${_a_data.deviceID} is NEW`);
            _g_wm_list[_a_data.deviceID] = createWM();
            _g_wm_list[_a_data.deviceID] = {
                _m_wm_id                : _a_data.deviceID,
            }
        }
        // clear cached sock.id
        else if(_g_wm_list[_a_data.deviceID]._m_sock_id)
        {
          // _g_wm_sock_list[_g_wm_list[_a_data.deviceID]._m_sock_id] = undefined;
            delete _g_wm_sock_list[_g_wm_list[_a_data.deviceID]._m_sock_id];
        }

        // update sock.id
        _g_wm_list[_a_data.deviceID]._m_sock_id = _a_socket.id;
        _g_wm_sock_list[_a_socket.id] = _a_data.deviceID;

        if(!_a_data.operate_state) _g_wm_list[_a_data.deviceID]._m_state = WMState.ONLINE;
        else _g_wm_list[_a_data.deviceID]._m_state = _a_data.operate_state;

        // if state is ONLINE: clear fact_used_id
        if(_g_wm_list[_a_data.deviceID]._m_state == WMState.ONLINE)
        {
          _g_wm_list[_a_data.deviceID]._m_fact_used_id = undefined;
        }
        // wm reconnect when RUNNING. get the last _m_fact_used_id record in order to update later.
        // else if(_g_wm_list[_a_data.deviceID]._m_state == WMState.RUNNING)
        // {
        //   var _l_db = new _g_sqlite3.Database(_g_db_file_name);
        //   _l_db.serialize(function() {
        //     let _l_query = `SELECT id, used_at FROM washing_machine_used WHERE wm_id = ${_a_data.deviceID} ORDER BY id DESC LIMIT 1`;
        //     _l_db.each(_l_query, function(_a_err, _a_row) {

        //       if(_a_err) console.log(_a_err);
        //       else
        //       {
        //         console.log(util.inspect(_a_row, {showHidden: true, depth: null}));
        //         _g_wm_list[_a_data.deviceID]._m_row_id = _a_row.id;
        //         _g_wm_list[_a_data.deviceID]._m_used_at = _a_row.used_at;
        //       }
        //     });

        //     _l_db.close();
        //   });

        // }

        if(_a_callback) _a_callback({'request_type' : 'wm_register', 'code' : WMResCode.OK});
        else _a_socket.emit('response', {'request_type' : 'wm_register', 'code' : WMResCode.OK});

        _a_socket.broadcast.emit('html_bc_wm_state_change', { wm_info : { 'request_type' : 'wm_register', 'wm_id' : _a_data.deviceID, 'state' : _g_wm_list[_a_data.deviceID]._m_state, }});
        // _a_socket.broadcast.emit('html_bc_play_sound', { 'file' : 'resources/background.mp3' });
    })
    .on('mng_request_run', function(_a_data, _a_callback)
    {
        let _l_wm_id = _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] REQUEST RUN: wm-${_l_wm_id}`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        console.log(`${__file} [${__line}] OK`);
        // notify new card to browser
        _a_socket.broadcast.emit('html_bc_card_register', { 'card_id' : _a_data.cardID });

        if(_a_callback) _a_callback({'request_type' : 'mng_request_run', 'code' : WMResCode.OK});
        else _a_socket.emit('response', {'request_type' : 'mng_request_run', 'code' : WMResCode.OK});

    })
    .on('wm_ask_for_run', function(_a_data, _a_callback)
    {
        let _l_wm_id = _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] RUN: wm-${_l_wm_id}`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        // if card is invalid or is used already
        if(!_g_cards[_a_data.cardID] || _g_cards[_a_data.cardID]._m_used_at || !_g_wm_list[_l_wm_id])
        {
          console.log(`${__file} [${__line}] NOT MATCHED`);
          if(_a_callback) _a_callback({'request_type' : 'wm_ask_for_run', 'code' : WMResCode.DECLINERUN});
          else _a_socket.emit('response', {'request_type' : 'wm_ask_for_run', 'code' : WMResCode.DECLINERUN});
          return;
        }

        _g_cards[_a_data.cardID]._m_wm_id = _l_wm_id;
        _g_cards[_a_data.cardID]._m_used_at = new Date(Math.round(new Date().getTime() / 1000)); // convert to seconds
        _g_wm_list[_l_wm_id]._m_state = WMState.RUNNING;
        _g_wm_list[_l_wm_id]._m_last_run = _g_cards[_a_data.cardID]._m_used_at;

        if(_a_callback) _a_callback({'request_type' : 'wm_ask_for_run', 'code' : WMResCode.ALOWRUN, 'duration' : _g_cards[_a_data.cardID]._m_duration});
        else _a_socket.emit('response', {'request_type' : 'wm_ask_for_run', 'code' : WMResCode.ALOWRUN, 'duration' : _g_cards[_a_data.cardID]._m_duration});

        _g_wm_list[_l_wm_id]._m_fact_used_id = _g_cards[_a_data.cardID]._m_fact_used_id;

        _a_socket.broadcast.emit('html_bc_wm_state_change', {
          wm_info : {
            'wm_id' : _l_wm_id,
            'state' : WMState.RUNNING,
            'card_id' : _a_data.cardID,
            'last_run' : _g_cards[_a_data.cardID]._m_used_at,
          },
          card_info : {
            'card_id'   : _a_data.cardID,
            'wm_id'     : _l_wm_id,
            'used_at'   : _g_cards[_a_data.cardID]._m_used_at,
          }
        }) ;

        // sql update query
        let _l_is_done = _g_cards[_a_data.cardID]._m_is_paid ? 1 : 0;
        let _l_query = `UPDATE FactUsed SET user_id=${_g_cards[_a_data.cardID]._m_user_id}, cost=${_g_cards[_a_data.cardID]._m_cost}, duration=${_g_cards[_a_data.cardID]._m_duration}, wm_id='${_l_wm_id}', card_id='${_a_data.cardID}', used_at=DATETIME('${_g_cards[_a_data.cardID]._m_used_at.getTime()}', 'unixepoch'), is_paid=${_g_cards[_a_data.cardID]._m_is_paid}, is_done=${_l_is_done} WHERE fact_used_id=${_g_cards[_a_data.cardID]._m_fact_used_id}`;
        execute_sql(_g_db_file_name, _l_query, (_a_err) => {
            // remove _g_cards[_a_data.cardID]
            if(!_a_err && _l_is_done)
            {
                // _g_cards[_a_data.cardID] = undefined;
                delete _g_cards[_a_data.cardID];
            }
        });


        console.log(`${__file} [${__line}] Res OK`);
    })
    .on('wm_stop', function(_a_data, _a_callback)
    {
        let _l_wm_id = _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] [INF] STOP: wm-${_l_wm_id}`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        // Received WM_STOP with wm_state is not RUNNING
        if(!_g_wm_list[_l_wm_id] || _g_wm_list[_l_wm_id]._m_state != WMState.RUNNING)
        {
            console.log(`${__file} [${__line}] ${_l_wm_id} [ERROR] Received WM_STOP: id or state is invalid`);
            if(_a_callback) _a_callback({'request_type' : 'wm_stop', 'code' : WMResCode.NOK});
            else _a_socket.emit('response', {'request_type' : 'wm_stop', 'code' : WMResCode.NOK});
            return;
        }

        // clear fact_used_id
        _g_wm_list[_l_wm_id]._m_fact_used_id = undefined;
        _g_wm_list[_l_wm_id]._m_state = WMState.ONLINE;

        if(_a_callback) _a_callback({'request_type' : 'wm_stop', 'code' : WMResCode.OK});
        else _a_socket.emit('response', {'request_type' : 'wm_stop', 'code' : WMResCode.OK});

        _a_socket.broadcast.emit('html_bc_wm_state_change', {
          wm_info : {
            'wm_id' : _l_wm_id,
            'state' : _g_wm_list[_l_wm_id]._m_state,
            'last_run' : _g_wm_list[_l_wm_id]._m_last_run,
          }
        }) ;

        console.log(`${__file} [${__line}] Res OK`);
    })
    .on('disconnect', () =>
    {
        let _l_wm_id = _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] [INF] DISCONNECT wm-${_l_wm_id}`);

        if(!_l_wm_id || !_g_wm_list[_l_wm_id]) return;

        // set state to offline
        _g_wm_list[_l_wm_id]._m_state = WMState.OFFLINE;

        _a_socket.broadcast.emit('html_bc_wm_state_change', {
          wm_info : {
            'wm_id' : _l_wm_id,
            'state' : _g_wm_list[_l_wm_id]._m_state,
            'last_run' : _g_wm_list[_l_wm_id]._m_last_run,
          }
        }) ;

        // clear sock.id
        // _g_wm_sock_list[_a_socket.id] = undefined;
        delete _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] Res OK`);

    })
    .on('timeout', () =>
    {
        let _l_wm_id = _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] [INF] TIMEOUT wm-${_l_wm_id}`);

        if(!_l_wm_id || !_g_wm_list[_l_wm_id]) return;

        // set state to offline
        _g_wm_list[_l_wm_id]._m_state = WMState.OFFLINE;

        _a_socket.broadcast.emit('html_bc_wm_state_change', {
          wm_info : {
            'wm_id' : _l_wm_id,
            'state' : _g_wm_list[_l_wm_id]._m_state,
            'last_run' : _g_wm_list[_l_wm_id]._m_last_run,
          }
        }) ;

        // clear sock.id
        // _g_wm_sock_list[_a_socket.id] = undefined;
        delete _g_wm_sock_list[_a_socket.id];
        console.log(`${__file} [${__line}] Res OK`);
    })
    .on('browser_save_user', (_a_data, _a_callback) =>
    {
        console.log(`${__file} [${__line}] [INF] Recieved browser_save_user`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        if(!_a_data._m_name || !_a_data._m_phone)
        {
            console.log(`${__file} [${__line}] [ERR] MUST HAVE USER NAME AND PHONE`);
            if(_a_callback) _a_callback(1);
            return;
        }

        // update
        // if(_a_data._m_request === 'update')
        // {
        //     let _l_query = `UPDATE User SET name='${_a_data._m_name}', phone='${_a_data._m_phone}' WHERE user_id=${_a_data._m_user_id}`;
        //     execute_sql(_g_db_file_name, _l_query);
        // }
        // insert
        // else
        // {
            let _l_query = `INSERT INTO User (name, phone) VALUES ('${_a_data._m_name}', '${_a_data._m_phone}')`;
            execute_sql(_g_db_file_name, _l_query, _a_callback);
        // }
    })
    .on('browser_update_card', (_a_data, _a_callback) =>
    {
        console.log(`${__file} [${__line}] [INF] Recieved browser_save_card`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        if(!_a_data._m_user_id || !_a_data._m_name || !_a_data._m_phone || !_a_data._m_cost || !_a_data._m_duration || !_a_data._m_card_id || !_a_data._m_fact_used_id)
        {
            console.log(`${__file} [${__line}] [ERR] Must have user_id, card_id, cost, duration, name, phone and fact_used_id`);
            if(_a_callback) _a_callback(1);
            return;
        }

        // update
        if(_g_cards[_a_data._m_card_id])
        {
            // check is_done
            let _l_is_done = (_a_data._m_is_paid && _g_cards[_a_data._m_card_id]._m_used_at) ? 1 : 0;

            let _l_query = `UPDATE FactUsed SET user_id=${_a_data._m_user_id}, cost=${_a_data._m_cost}, duration=${_a_data._m_duration}, card_id='${_a_data._m_card_id}', is_paid=${_a_data._m_is_paid}, is_done=${_l_is_done} WHERE fact_used_id=${_a_data._m_fact_used_id}`;
            execute_sql(_g_db_file_name, _l_query, (_a_err) =>
            {
                if(_a_err) { _a_callback(1); return; }

                // remove FactUsed record.
                if(_l_is_done)
                {
                    // _g_cards[_a_data._m_card_id] = undefined;
                    delete _g_cards[_a_data._m_card_id];
                }
                // update cached data.
                else
                {
                    _g_cards[_a_data._m_card_id]._m_user_id = _a_data._m_user_id;
                    _g_cards[_a_data._m_card_id]._m_cost = _a_data._m_cost;
                    _g_cards[_a_data._m_card_id]._m_name = _a_data._m_name;
                    _g_cards[_a_data._m_card_id]._m_phone = _a_data._m_phone;
                    _g_cards[_a_data._m_card_id]._m_duration = _a_data._m_duration;
                    _g_cards[_a_data._m_card_id]._m_card_id = _a_data._m_card_id;
                    _g_cards[_a_data._m_card_id]._m_is_paid = _a_data._m_is_paid;
                }

                console.log(_g_cards[_a_data._m_card_id]);
                if(_a_callback) _a_callback(0);
            });
        }
        else
        {
            console.log(`${__file} [${__line}] [ERR] TRYING TO UPDATE INVALID RECORD`);
            if(_a_callback) _a_callback(1);
        }


    }).on('browser_insert_card', (_a_data, _a_callback) =>
    {
        console.log(`${__file} [${__line}] [INF] Recieved browser_insert_card`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        if(!_a_data._m_user_id || !_a_data._m_name || !_a_data._m_phone || !_a_data._m_cost || !_a_data._m_duration || !_a_data._m_card_id)
        {
            console.log(`${__file} [${__line}] [ERR] Must have user_id, card_id, cost, duration, name and phone`);
            if(_a_callback) _a_callback(1);
            return;
        }

        let _l_query = `INSERT INTO FactUsed (user_id, cost, duration, card_id, is_paid) VALUES (${_a_data._m_user_id}, ${_a_data._m_cost}, ${_a_data._m_duration}, '${_a_data._m_card_id}', ${_a_data._m_is_paid})`;
        execute_sql(_g_db_file_name, _l_query, (_a_err, _a_fact_used_id) =>
        {
            if(!_a_err)
            {
                _g_cards[_a_data._m_card_id] = {
                    _m_fact_used_id     : _a_fact_used_id,
                    _m_user_id          : _a_data._m_user_id,
                    _m_name             : _a_data._m_name,
                    _m_phone            : _a_data._m_phone,
                    _m_cost             : _a_data._m_cost,
                    _m_duration         : _a_data._m_duration,
                    _m_card_id          : _a_data._m_card_id,
                    _m_is_paid          : _a_data._m_is_paid,
                }

            }

            if(_a_callback) _a_callback(_a_err, _a_fact_used_id);
        });

    }).on('browser_delete_card', (_a_data, _a_callback) =>
    {
        console.log(`${__file} [${__line}] [INF] Recieved browser_dedlete_card`);
        console.log(util.inspect(_a_data, {showHidden: true, depth: null}));

        if(!_a_data._m_fact_used_id)
        {
            console.log(`${__file} [${__line}] [ERR] Must have fact_used_id`);
            return;
        }

        // let _l_cb = _a_callback;
        // console.log(`${__file} [${__line}] [DBG] ${_l_cb}`);
        let _l_card_id = 0;
        let _l_is_paid = 0;
        let _l_used_at = 0;

        // check data in db is matched.
        let _l_query = `SELECT card_id, is_paid, used_at FROM FactUsed WHERE fact_used_id=${_a_data._m_fact_used_id}`;
        console.log(`${__file} [${__line}] [DBG] execute: ${_l_query}`);

        let _l_db = new _g_sqlite3.Database(_g_db_file_name);
        _l_db.serialize(function() {

            _l_db.each(_l_query, function(_a_err, _a_row) {

                if(_a_err) console.log(`[ERR] _a_err`);
                else
                {
                    console.log(`${__file} [${__line}] [DBG]`);

                    _l_card_id = _a_row.card_id;
                    _l_is_paid = _a_row.is_paid;
                    _l_used_at = _a_row.used_at;

                    console.log(util.inspect(_a_row, {showHidden: true, depth: null}));

                    if(_l_card_id && _g_cards[_l_card_id] && !_l_is_paid && !_l_used_at)
                    {
                        _l_query = `DELETE FROM FactUsed WHERE fact_used_id = ${_a_data._m_fact_used_id}`;
                        console.log(`${__file} [${__line}] [DBG] execute: ${_l_query}`);
                        _l_db.run(_l_query, (_a_err) => {
                            console.log(_a_err);
                            if(!_a_err)
                            {
                                delete _g_cards[_l_card_id];
                            }

                            // console.log(`${__file} [${__line}] [DBG] ${_l_cb}`);
                            if(_a_callback) _a_callback(_a_err);
                        });
                    }
                    else if(_a_callback) _a_callback(1);
                }
            }, function() {



                _l_db.close();
            });

        });


    });
};

module.exports = svrio;



