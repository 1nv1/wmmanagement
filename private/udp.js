/////////////////////////////TCP_UDP///////////////////////////////////////////

var dgram = require('dgram');

var fs = require('fs');

var svrBC = dgram.createSocket('udp4');

svrBC.on('listening', () => {
    var address = svrBC.address();
    console.log(`${__file} [${__line}] UDP is listening on ${address.address} : ${address.port}`);
});

svrBC.on('message', (buff, remote) => {
    console.log(` ${__file} [${__line}] ${remote.address} : ${remote.port} - ${buff}`);
    var date = new Date();
    fs.appendFile('log.txt', `${date} ${remote.address} : ${remote.port} : ${buff}\n`, function (err) {
      if (err) throw err;
      // console.log('${__file} [${__line} Saved!');
    });

    buff = Buffer.allocUnsafe(1);
    buff.writeInt8(WMResCode.OK,0);
    // buff = Buffer.from(`${WMResCode.OK}`);

    svrBC.send(buff, 0, buff.length, remote.port, remote.address, (err, bytes) => {
        if (err) throw err;
        console.log(`${__file} [${__line}] UDP buff sent to ${remote.address} : ${remote.port}`);
    });
});


module.exports = svrBC;

