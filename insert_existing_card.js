const util = require('util');

var io_mng = require('socket.io-client');
var socket_mng = io_mng('http://localhost:9696');

socket_mng.on('connect', () => {
	console.log(`CONNECTED`);
})
.on('response', (data) => {
	console.log(util.inspect(data, {showHidden: true, depth: null}));
});


var card_id = [
	'00100',
	'00200',
	'00110',
	'00101',
	'00111',
];

socket_mng.emit('mng_request_run', {'cardID' : card_id[0] }, (err) => {
	console.log(err);

	setTimeout(() => {
		socket_mng.emit('mng_request_run', {'cardID' : card_id[1] }, (err) => {
			console.log(err);

			setTimeout(() => {
				socket_mng.emit('mng_request_run', {'cardID' : card_id[2] }, (err) => {
					console.log(err);

					// setTimeout(() => {
					// 	socket_mng.emit('mng_request_run', {'cardID' : card_id[3] }, (err) => {
					// 		console.log(err);

							socket_mng.close();

					// 	});
					// }, 3000);
				});
			}, 500);

		});
	}, 500);

});
